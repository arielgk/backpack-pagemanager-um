<?php

namespace Backpack\PageManager\app\Models;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Backpack\PageManager\app\Models\Page;

class LibraryScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        $parentLibraryId = Page::where('name', '=', 'Biblioteca')->pluck('id')->first();
        $parentLibraryServiceId = Page::where('name', '=', 'Servicios de Biblioteca')->pluck('id')->first();

        $builder->where('parentId', '=', $parentLibraryId)->orWhere('parentId', '=', $parentLibraryServiceId);
    }
}